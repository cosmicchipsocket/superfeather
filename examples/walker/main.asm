
.p816   ; 65816 processor
.a16
.i16
.smart

.import Gameloop, sceneInitFunc, sceneThinkFunc, WalkerSceneInit, WalkerSceneThink

.export GameMain, SpcDriver

.segment "DATA3"

SpcDriver:
    .incbin "examples/walker/data/spc700.bin"

.segment "CODE0"


; ============================================================================
; GameMain is called once the engine has finished its own initialization. This space is for your game
;  to perform its own setup.
; ============================================================================
GameMain:
    
    ; Set your initial scenes here by writing to sceneInitFunc and sceneThinkFunc
    lda #.loword(WalkerSceneInit)
    sta sceneInitFunc
    lda #.loword(WalkerSceneThink)
    sta sceneThinkFunc

_Forever:
    jsr Gameloop
    bra _Forever
