
.p816   ; 65816 processor
.a16
.i16
.smart

; Most source files should start with the above series of directives. This tells ca65 that we are
;  programming for the 65816 and that it should track use of rep/sep to determine the register sizes,
;  but that they should default to 16-bit.

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/gameobjects.inc"
.include "../../inc/engine/graphics.inc"

.import CreateObject, DeleteObject, MoveObject, AddSimpleSprite, RandomByte, objThinker, objPosX, objPosY, objPosSubX, objPosSubY, objVelX, objVelY, objAttribute, objAnimDef
.export BallChar, BallChar_Len, SpawnBall, Ball_Small, Ball_Large

.segment "CODE0"

;                     Name,             X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Ball_Large,       $fff8, $fff0, SPRITE_LARGE, $01, %00100000, 0
Define_SpriteDef      Ball_Small,       $fffc, $fff8, SPRITE_SMALL, $00, %00100000, 0

BallChar:
    .INCBIN "examples/bounceballs/data/ball.pic"
BallChar_Len = * - BallChar


; ============================================================================
; Creates a new ball in the corner of the screen and initializes the relevant variables.
; ============================================================================
SpawnBall:
    php
    rep #$70
    
    phy
    
    ; Use object pool 0
    ldy #$0000
    ; Try to get an object index from the pool
    jsr CreateObject
    ; If overflow CPU bit is set, we ran out of objects.
    bvs _NotCreated
    ; Otherwise, our X is set to the new object's index. Let's set some variables.
    
    ; Initial position
    sep #$20
    stz objPosSubX, x
    stz objPosSubY, x
    rep #$20
    lda #$00f8
    sta objPosX, x
    lda #$0010
    sta objPosY, x
    
    ; Draw a random number for initial leftward velocity.
    jsr RandomByte
    ; The random byte is positive, so subtract to make it negative.
    sec
    sbc #$1ff
    sta objVelX, x
    ; Y velocity, sprite attribute
    stz objVelY, x
    stz objAttribute, x
    
    ; Give this object a behavior to run every frame
    lda #.loword(BallThink)
    sta objThinker, x
    
    ; Give this object the sprite that was stored in Y when the function was called.
    pla
    sta objAnimDef, x
    plp
    rts

    ; We hit the object limit
_NotCreated:
    pla
    plp
    rts


; ============================================================================
; The thinker function assigned to all ball objects.
; ============================================================================
BallThink:
    ; Add a constant amount of velocity to create gravity.
    lda objVelY, x
    clc
    adc #$40
    sta objVelY, x
    
    ; Now move this object according to its X and Y velocities.
    jsr MoveObject
    
    ; Check if this object hit the ground. If so, bounce!
    lda objPosY, x
    cmp #$e0
    bcs _BallBounce

_BallBounceDone:
    ; Draw this object using the spritedef it was assigned.
    ldy objAnimDef, x
    jsr AddSimpleSprite
    
    ; If the CPU overflow bit is set, the sprite wasn't added.
    ;  From this, we assume the object was outside the screen and delete it.
    bvs _BallOutOfRange
    ; Go to next object
    Macro_NextThinker

 _BallOutOfRange:
    ; We're deleting the object, returning its slot to the pool.
    jsr DeleteObject
    ; Go to next object
    Macro_NextThinker

_BallBounce:
    ; Get the existing Y velocity
    lda objVelY, x
    ; Don't bounce if it's traveling upward
    bmi _BallBounceDone
    ; Invert the Y velocity
    sec
    sbc objVelY, x
    sbc objVelY, x
    ; Slightly reduce the strength of the velocity on bounce
    clc
    adc #$100
    ; Set the velocity
    sta objVelY, x
    ; Push the ball out of the ground.
    lda #$e0
    sta objPosY, x
    bra _BallBounceDone
