
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/gameobjects.inc"
.include "../../inc/engine/graphics.inc"
.include "../../inc/engine/utils.inc"

.import CreateObject, AddMetasprite, AddMetaspriteBoundingBox, DespawnObjectIfOutside, RandomByte, objPosX, objPosY, objThinker
.export SpawnBurst, BurstThinker, sparsePlacement, disableBoundsCheck

.segment "LORAM"
sparsePlacement: .res 2
disableBoundsCheck: .res 2

.segment "CODE0"

; ============================================================================
; Spritedefs are defined below
; ============================================================================

;                     Name,                       X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Sprite_Burst_1,             $0000, $0000, SPRITE_LARGE, $01, %00100000, Sprite_Burst_2
Define_SpriteDef      Sprite_Burst_2,             $0010, $0000, SPRITE_LARGE, $01, %01100000, Sprite_Burst_3
Define_SpriteDef      Sprite_Burst_3,             $0000, $0010, SPRITE_LARGE, $01, %10100000, Sprite_Burst_4
Define_SpriteDef      Sprite_Burst_4,             $0010, $0010, SPRITE_LARGE, $01, %11100000, 0


; ============================================================================
; BoundingBoxDefs are used to determine whether the entire metasprite is
;  outside the view and therefore should not be drawn. The data pointer is set
;  to the Spritedef associated with this BoundingBoxDef.
; ============================================================================

;                      Name,                       left,   top,    width,  height, data pointer
Define_BoundingBoxDef  BoundingBox_Burst,          $0000,  $0000,  $0020,  $0020,  Sprite_Burst_1

; ============================================================================
; Creates a new Burst object
; ============================================================================
SpawnBurst:
    php
    rep #$70
    
    ; Use object pool 0
    ldy #$00
    ; Try to get an object index from the pool
    jsr CreateObject
    ; If overflow CPU bit is set, we ran out of objects.
    bvs _NotCreated
    ; Otherwise, our X is set to the new object's index. Let's set some variables.
    
    ; Give this object a behavior to run every frame
    lda #.loword(BurstThinker)
    sta objThinker, x
    
    ; Initial position
    jsr RandomByte
    ldy sparsePlacement
    beq _PlaceDenseX
    
    ; Spread out the objects along X if using sparse placement
    asl
    asl
_PlaceDenseX:
    sta objPosX, x
    
    jsr RandomByte
    ldy sparsePlacement
    beq _PlaceDenseY
    
    ; Spread out the objects along Y if using sparse placement
    asl
    asl
_PlaceDenseY:
    sta objPosY, x
    
    plp
    rts

    ; We hit the object limit
_NotCreated:
    plp
    rts

; ============================================================================
; The thinker function associated with this object. Called once a frame for
;  all objects of this type.
; ============================================================================
BurstThinker:
    ; Display this sprite onscreen
    lda disableBoundsCheck
    bne _BoundsCheckDisabled
    
    ; Do full bounding box check before drawing metasprite.
    ldy #.loword(BoundingBox_Burst)
    jsr AddMetaspriteBoundingBox
    
    bra _SpriteDrawDone

_BoundsCheckDisabled:
    ; Don't do bounding box check. Every hardware sprite is considered if the
    ;  whole object is off-screen (slower)
    ldy #.loword(Sprite_Burst_1)
    jsr AddMetasprite

    bra _SpriteDrawDone

_SpriteDrawDone:
    ; Go to next object
    Macro_NextThinker
