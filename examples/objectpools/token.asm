
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/gameobjects.inc"
.include "../../inc/engine/graphics.inc"

.import CreateObject, DeleteObject, AddSimpleSprite, AnimateWithCallback, SetAnimation, objPosX, objPosY, objPosSubX, objPosSubY, objAnimDef, objThinker
.export SpawnToken, TokenThinker

.segment "CODE0"

; ============================================================================
; Spritedefs are defined below
; ============================================================================

;                     Name,                       X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Sprite_Player_Dragon,       $0000, $fff0, SPRITE_LARGE, $00, %00100000, 0
Define_SpriteDef      Sprite_Gem,                 $0000, $0000, SPRITE_SMALL, $04, %00100010, 0
Define_SpriteDef      Sprite_Spikeball,           $0000, $0000, SPRITE_SMALL, $06, %00100010, 0
Define_SpriteDef      Sprite_Sparkle_1,           $0000, $0000, SPRITE_SMALL, $24, %00100100, 0
Define_SpriteDef      Sprite_Sparkle_2,           $0000, $0000, SPRITE_SMALL, $26, %00100100, 0
Define_SpriteDef      Sprite_Sparkle_3,           $0000, $0000, SPRITE_SMALL, $28, %00100100, 0
Define_SpriteDef      Sprite_Sparkle_4,           $0000, $0000, SPRITE_SMALL, $2a, %00100100, 0
Define_SpriteDef      Sprite_Sparkle_5,           $0000, $0000, SPRITE_SMALL, $2c, %00100100, 0
Define_SpriteDef      Sprite_Sparkle_6,           $0000, $0000, SPRITE_SMALL, $2e, %00100100, 0

; ============================================================================
; The animdefs are assigned to objects according to the spawn IDs.
; ============================================================================

;                     Name,                      delay, next anim,          callback
Define_AnimDef        Anim_Player_Dragon,        0,     Anim_Player_Dragon, 0
.word .loword(Sprite_Player_Dragon)
Define_AnimDef        Anim_Gem,                  0,     Anim_Gem,           0
.word .loword(Sprite_Gem)
Define_AnimDef        Anim_Spikeball,            0,     Anim_Spikeball,     0
.word .loword(Sprite_Spikeball)
Define_AnimDef        Anim_Sparkle_1,            4,     Anim_Sparkle_2,     0

; ============================================================================
; The sparkle animation sequence. After the last frame we make a call to DeleteObject
;  so that the object returns itself to the object pool and a new one can take its place.
; ============================================================================
.word .loword(Sprite_Sparkle_1)
Define_AnimDef        Anim_Sparkle_2,            4,     Anim_Sparkle_3,     0
.word .loword(Sprite_Sparkle_2)
Define_AnimDef        Anim_Sparkle_3,            4,     Anim_Sparkle_4,     0
.word .loword(Sprite_Sparkle_3)
Define_AnimDef        Anim_Sparkle_4,            4,     Anim_Sparkle_5,     0
.word .loword(Sprite_Sparkle_4)
Define_AnimDef        Anim_Sparkle_5,            4,     Anim_Sparkle_6,     0
.word .loword(Sprite_Sparkle_5)
Define_AnimDef        Anim_Sparkle_6,            4,     Anim_Sparkle_End,   0
.word .loword(Sprite_Sparkle_6)
Define_AnimDef        Anim_Sparkle_End,          4,     Anim_Sparkle_End,   DeleteObject
.word .loword(Sprite_Sparkle_6)

_ObjectPoolAssignment:
.word $0000 ; SPAWNID_PLAYER_DRAGON
.word $0002 ; SPAWNID_GEM
.word $0004 ; SPAWNID_SPIKEBALL
.word $0006 ; SPAWNID_SPARKLE

_ObjectAnimDefAssignment:
.word .loword(Anim_Player_Dragon)
.word .loword(Anim_Gem)
.word .loword(Anim_Spikeball)
.word .loword(Anim_Sparkle_1)

; ============================================================================
; Creates a new Token object using the spawn ID passed in via X index register
; ============================================================================
SpawnToken:
    php
    rep #$70
    
    phy
    
    ; Use object pool according to the object ID given
    lda _ObjectPoolAssignment, y
    tay
    ; Try to get an object index from the pool
    jsr CreateObject
    ; If overflow CPU bit is set, we ran out of objects.
    bvs _NotCreated
    ; Otherwise, our X is set to the new object's index. Let's set some variables.
    
    ; Divide by 3, for example clarity (object indices are multiples of 3, but we want to show as
    ;  multiples of 1). Because this is a delayed operation, we do it now and read the result later!
    stx $4204
    sep #$20
    lda #$03
    sta $4206
    rep #$20
    
    ; Give this object a behavior to run every frame
    lda #.loword(TokenThinker)
    sta objThinker, x
    
    ; Give this object the appropriate sprite based on the object ID.
    ply
    lda _ObjectAnimDefAssignment, y
    tay
    jsr SetAnimation
    
    ; Initial position
    ldy $4214
    
    ; Make the object cascade down the screen
    tya
    asl
    asl
    asl
    asl
    and #$ff
    sta objPosX, x
    
    tya
    and #$f0
    clc
    adc #$10
    sta objPosY, x
    
    plp
    rts

    ; We hit the object limit
_NotCreated:
    ply
    plp
    rts

; ============================================================================
; The thinker function associated with this object. Called once a frame for
;  all objects of this type.
; ============================================================================
TokenThinker:
    ; Process any animation
    jsr AnimateWithCallback
    ; Our sprite is at the end of the current animdef. Find it and put it in Y.
    ldy objAnimDef, x
    ; Here we get the actual spritedef out of the data structure
    lda ANIMDEF_DATAEX, y
    tay
    ; Display this sprite onscreen
    jsr AddSimpleSprite
    ; Go to next object
    Macro_NextThinker
