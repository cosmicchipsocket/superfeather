
return
{
    fileList =
    {
        -- Engine files
        "src/engine/init.asm",
        "src/engine/utils.asm",
        "src/engine/initsnes.asm",
        "src/engine/crashhandler.asm",
        "src/engine/gameloop.asm",
        "src/engine/audio.asm",
        "src/engine/dma.asm",
        "src/engine/graphics.asm",
        "src/engine/dmamanager.asm",
        "src/engine/gameobjects.asm",
        
        -- Game files
        "examples/misc/vramslottest/header.asm",
        "examples/misc/vramslottest/main.asm",
        "examples/misc/vramslottest/vramslottest.asm",
        "examples/misc/vramslottest/objectblock.asm",
    },
    target = "superfeather-ex-vramslottest",
    linkerConfig = "examples/config-ca65.cfg"
}
