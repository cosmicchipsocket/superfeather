
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../../inc/engine/constants.inc"
.include "../../../inc/engine/dma.inc"
.include "../../../inc/engine/graphics.inc"

.import SetupPpuRegisters:far, sceneTics, inputTap, inputRaw, screenBrightness, switchScene, SetObjectPools, UpdateObjects, BeginSprites, FinalizeSprites, VramAllocateSlot, VramDeallocateSlot, SpawnBlock, DespawnBlock

.export VramSlotTestSceneInit, VramSlotTestSceneThink


.segment "DATA2"

VramSlotTestPal:
    .INCBIN "examples/misc/vramslottest/data/vramslottest.pal"
VramSlotTestPal_Len = * - VramSlotTestPal

HudChar:
    .INCBIN "examples/misc/vramslottest/data/hud.pic2"
HudChar_Len = * - HudChar

HudTilemap:
    .INCBIN "examples/misc/vramslottest/data/hud.map"
HudTilemap_Len = * - HudTilemap

SpacesChar:
    .INCBIN "examples/misc/vramslottest/data/spaces.pic"
SpacesChar_Len = * - SpacesChar

BlocksChar:
    .INCBIN "examples/misc/vramslottest/data/blocks.pic"
BlocksChar_Len = * - BlocksChar

SpacesTilemap:
    .INCBIN "examples/misc/vramslottest/data/spaces.map"
SpacesTilemap_Len = * - SpacesTilemap

 
.segment "CODE0"

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0, %00001001, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_3000 | PPU_BG24CHARADDR_3000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00010111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


_ObjectPoolDef:
    .word $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
VramSlotTestSceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters
    
    ; We are managing the object IDs ourselves, so disable CreateObject
    ldx #.loword(_ObjectPoolDef)
    jsr SetObjectPools

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette VramSlotTestPal, #0, #VramSlotTestPal_Len
    Macro_LoadVRAM HudChar, #$2000, #HudChar_Len
    Macro_LoadVRAM HudTilemap, #$7800, #HudTilemap_Len
    Macro_LoadVRAM BlocksChar, #$0000, #BlocksChar_Len
    Macro_LoadVRAM SpacesChar, #$3000, #SpacesChar_Len
    Macro_LoadVRAM SpacesTilemap, #$6000, #SpacesTilemap_Len
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
VramSlotTestSceneThink:
    php
    
    sep #$20
    lda #%00001111
    sta screenBrightness
    
    rep #$30

    lda inputTap
    bit #INPUT_A
    beq _NoA
    ldy #VRAM_BLOCK_SIZE_1
    jsr _DoSpawnBlock
    
_NoA:
    lda inputTap
    bit #INPUT_B
    beq _NoB
    ldy #VRAM_BLOCK_SIZE_2
    jsr _DoSpawnBlock
    
_NoB:
    lda inputTap
    bit #INPUT_X
    beq _NoX
    ldy #VRAM_BLOCK_SIZE_3
    jsr _DoSpawnBlock
    
_NoX:
    lda inputTap
    bit #INPUT_Y
    beq _NoY
    ldy #VRAM_BLOCK_SIZE_4
    jsr _DoSpawnBlock
    
_NoY:
    lda inputRaw
    bit #INPUT_L
    beq _NoL
    lda sceneTics
    and #$1f
    tay
    jsr _DoReallocBlock

_NoL:
    lda inputRaw
    bit #INPUT_R
    beq _NoR
    lda sceneTics
    and #$1f
    tay
    jsr _DoDespawnBlock

_NoR:
    lda inputTap
    bit #INPUT_SELECT
    beq _NoSelect
    ; Restart the scene
    lda #$01
    sta switchScene

_NoSelect:
    jsr BeginSprites
    jsr UpdateObjects
    jsr FinalizeSprites
    
    plp
    rts

_DoSpawnBlock:
    lda #$0008
    jsr VramAllocateSlot
    bvs _DontSpawnBlock
    jsr SpawnBlock
_DontSpawnBlock:
    rts

_DoDespawnBlock:
    jsr VramDeallocateSlot
    bvs _DontSpawnBlock
    jsr DespawnBlock
    rts

_DoReallocBlock:
    jsr VramDeallocateSlot
    bvs _DontSpawnBlock
    jsr DespawnBlock
    jmp _DoSpawnBlock
    
