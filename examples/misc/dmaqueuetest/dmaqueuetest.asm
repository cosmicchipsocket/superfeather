
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../../inc/engine/constants.inc"
.include "../../../inc/engine/dma.inc"
.include "../../../inc/engine/graphics.inc"

.import SetupPpuRegisters:far, screenBrightness, dmaTableSrc, dmaTableSrcBank, dmaTableDest, dmaQueueMaxSize, inputRaw, DMATryQueueAdd, RandomByte

.export DmaQueueSceneInit, DmaQueueSceneThink

.segment "DATA2"

DmaQueuePal:
    .INCBIN "examples/misc/dmaqueuetest/data/dmaqueuetest.pal"
DmaQueuePal_Len = * - DmaQueuePal

DmaQueueChar:
    .INCBIN "examples/misc/dmaqueuetest/data/sequential.pic"
DmaQueueChar_Len = * - DmaQueueChar

DmaQueueDragon:
    .INCBIN "examples/misc/dmaqueuetest/data/dragon.pic"
DmaQueueDragon_Len = * - DmaQueueDragon

DmaQueueTilemap:
    .INCBIN "examples/misc/dmaqueuetest/data/sequential.map"
DmaQueueTilemap_Len = * - DmaQueueTilemap

 
.segment "CODE0"

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0, %00001001, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00010001, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
DmaQueueSceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette DmaQueuePal, #0, #DmaQueuePal_Len
    Macro_LoadVRAM DmaQueueChar, #$2000, #DmaQueueChar_Len
    Macro_LoadVRAM DmaQueueTilemap, #$6000, #DmaQueueTilemap_Len
    
    rep #$30
    
    lda #$1000
    sta dmaQueueMaxSize
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
DmaQueueSceneThink:
    php
    
    sep #$20
    lda #%00001111
    sta screenBrightness
    
    rep #$30
    
    ; Transfer loop, to try and transfer each of the 32 slots.
    ldy #$0000
_DoQueueSlotsLoop:
    ; Randomly don't transfer some graphics if a button is held
    lda #$200
    ldx inputRaw
    beq _ForceTransfer
    jsr RandomByte
    cmp #$c0
    bcc _NoQueueTransfer
    ; Randomize the transfer size
    jsr RandomByte
    asl
_ForceTransfer:
    phy
    jsr DMATryQueueAdd
    ply
    bvs _NoQueueTransfer
    
    ; Our source bank
    lda #.bank(DmaQueueDragon)
    sta dmaTableSrcBank, x
    ; The char we want to stream to VRAM. Randomize offset so we can see who updates.
    jsr RandomByte
    xba
    lsr
    and #$03ff
    adc #.loword(DmaQueueDragon)
    sta dmaTableSrc, x
    ; The VRAM address to put it in.
    tya
    xba
    lsr
    clc
    adc #$2000
    sta dmaTableDest, x
    
_NoQueueTransfer:
    
    iny
    iny
    cpy #64
    bcc _DoQueueSlotsLoop
    
    plp
    rts
