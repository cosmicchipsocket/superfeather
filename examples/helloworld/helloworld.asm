
.p816   ; 65816 processor
.a16
.i16
.smart

; Most source files should start with the above series of directives. This tells ca65 that we are
;  programming for the 65816 and that it should track use of rep/sep to determine the register sizes,
;  but that they should default to 16-bit.

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/dma.inc"
.include "../../inc/engine/graphics.inc"

; Import/export statements are how you access symbols defined in other modules. Here we just need to
;  call a function and set a variable that isn't defined here, so we include those. We define
;  HelloWorldInit and HelloWorldThink, which are used during game initialization, so we export them
;  to make them usable elsewhere.

.import SetupPpuRegisters:far, screenBrightness

.export HelloWorldInit, HelloWorldThink

.segment "DATA2"

; Here we include several bits of data: the palette to use, the letter graphics, and a tilemap that
;  we use to lay out our letters. During initialization we upload these to the PPU.

HelloPal:
    .INCBIN "examples/helloworld/data/char.pal"
HelloPal_Len = * - HelloPal

HelloChar:
    .INCBIN "examples/helloworld/data/char.pic2"
HelloChar_Len = * - HelloChar

HelloTilemap:
    .INCBIN "examples/helloworld/data/char.map"
HelloTilemap_Len = * - HelloTilemap

 
.segment "CODE0"

; These values are used to initialize the PPU registers, so we can set up the background mode and
;  address offsets we need, among other things. See documentation on the PPU registers for more
;  information.

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0, %00001001, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00011111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
HelloWorldInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette HelloPal, #0, #HelloPal_Len
    Macro_LoadVRAM HelloChar, #$2000, #HelloChar_Len
    Macro_LoadVRAM HelloTilemap, #$7800, #HelloTilemap_Len
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
HelloWorldThink:
    php
    
    ; Nothing much to do here. Normally we would call into the object processing functions
    ;  here, but since this example is so simple, all we're going to do is turn on the screen.
    sep #$20
    lda #%00001111
    sta screenBrightness
    
    plp
    rts
