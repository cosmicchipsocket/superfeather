
.p816   ; 65816 processor
.a16
.i16
.smart

.include "sounds.inc"
.include "../../inc/engine/constants.inc"
.include "../../inc/engine/gameobjects.inc"
.include "../../inc/engine/graphics.inc"

.export SpawnDragon

.import CreateObject, MoveObject, SetAnimation, AnimateWithCallback, AddSimpleSprite
.import inputRaw, objThinker, objPosX, objPosY, objPosSubX, objPosSubY, objVelX, objVelY, objAttribute, objAnimDef, objCustomA, objCustomB, spriteScrollX, spriteScrollY, soundAId

; Imports for DMA management
.import DMATryAddStrip4, dmaTableStripSrc, dmaTableStripSrcBank, dmaTableStripDest

.segment "CODE0"

; ============================================================================
; In terms of how graphics are stored, the big difference between this example and the previous "walker"
;  example is that in "walker", the char was basically a large static sprite sheet we uploaded into VRAM
;  at start. Here, we take a different approach by streaming sprites to VRAM via DMA:
;
; - Each sprite frame has its own separate file, and therefore, its own label. This lets us easily locate
;   the data we want to send via DMA transfer.
;
; - Due to the way that sprites are laid out in VRAM, each 32x32 frame has been transformed into a 128x8
;   image, with each 32x8 strip lined up. We then DMA each of these four strips in succession, since they
;   don't line up contiguously.
; ============================================================================
DragonCharS1:
    .INCBIN "examples/walker-dynamic/data/dragon/S1.pic"

DragonCharS2:
    .INCBIN "examples/walker-dynamic/data/dragon/S2.pic"

DragonCharSE1:
    .INCBIN "examples/walker-dynamic/data/dragon/SE1.pic"

DragonCharSE2:
    .INCBIN "examples/walker-dynamic/data/dragon/SE2.pic"

DragonCharSE3:
    .INCBIN "examples/walker-dynamic/data/dragon/SE3.pic"

DragonCharE1:
    .INCBIN "examples/walker-dynamic/data/dragon/E1.pic"

DragonCharE2:
    .INCBIN "examples/walker-dynamic/data/dragon/E2.pic"

DragonCharE3:
    .INCBIN "examples/walker-dynamic/data/dragon/E3.pic"

DragonCharNE1:
    .INCBIN "examples/walker-dynamic/data/dragon/NE1.pic"

DragonCharNE2:
    .INCBIN "examples/walker-dynamic/data/dragon/NE2.pic"

DragonCharNE3:
    .INCBIN "examples/walker-dynamic/data/dragon/NE3.pic"

DragonCharN1:
    .INCBIN "examples/walker-dynamic/data/dragon/N1.pic"

DragonCharN2:
    .INCBIN "examples/walker-dynamic/data/dragon/N2.pic"


; ============================================================================
; Individual hardware sprites to display are listed below. Unlike the previous "walker" demo, the tile
;  is always set to 0, because we're streaming the sprites into the same spot in VRAM.

; We also add an additional piece of data after the spritedef - the address of the sprite char we want
;  to stream into VRAM.
; ============================================================================
;                     Name,             X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Dragon_S1,        $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharS1)
Define_SpriteDef      Dragon_S2,        $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharS2)
Define_SpriteDef      Dragon_S3,        $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharS2)
Define_SpriteDef      Dragon_S4,        $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharS1)
Define_SpriteDef      Dragon_S5,        $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharS2)
Define_SpriteDef      Dragon_S6,        $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharS2)

Define_SpriteDef      Dragon_SE1,       $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE1)
Define_SpriteDef      Dragon_SE2,       $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE2)
Define_SpriteDef      Dragon_SE3,       $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE2)
Define_SpriteDef      Dragon_SE4,       $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE1)
Define_SpriteDef      Dragon_SE5,       $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE3)
Define_SpriteDef      Dragon_SE6,       $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharSE3)

Define_SpriteDef      Dragon_E1,        $ffea, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE1)
Define_SpriteDef      Dragon_E2,        $ffea, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE2)
Define_SpriteDef      Dragon_E3,        $ffea, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE2)
Define_SpriteDef      Dragon_E4,        $ffea, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE1)
Define_SpriteDef      Dragon_E5,        $ffea, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE3)
Define_SpriteDef      Dragon_E6,        $ffea, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharE3)

Define_SpriteDef      Dragon_NE1,       $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE1)
Define_SpriteDef      Dragon_NE2,       $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE2)
Define_SpriteDef      Dragon_NE3,       $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE2)
Define_SpriteDef      Dragon_NE4,       $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE1)
Define_SpriteDef      Dragon_NE5,       $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE3)
Define_SpriteDef      Dragon_NE6,       $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharNE3)

Define_SpriteDef      Dragon_N1,        $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharN1)
Define_SpriteDef      Dragon_N2,        $fff0, $ffef, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharN2)
Define_SpriteDef      Dragon_N3,        $fff0, $ffee, SPRITE_LARGE, $00, %00100000, 0
.word .loword(DragonCharN2)
Define_SpriteDef      Dragon_N4,        $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharN1)
Define_SpriteDef      Dragon_N5,        $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharN2)
Define_SpriteDef      Dragon_N6,        $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharN2)

Define_SpriteDef      Dragon_NW1,       $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE1)
Define_SpriteDef      Dragon_NW2,       $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE2)
Define_SpriteDef      Dragon_NW3,       $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE2)
Define_SpriteDef      Dragon_NW4,       $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE1)
Define_SpriteDef      Dragon_NW5,       $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE3)
Define_SpriteDef      Dragon_NW6,       $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharNE3)

Define_SpriteDef      Dragon_W1,        $fff6, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE1)
Define_SpriteDef      Dragon_W2,        $fff6, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE2)
Define_SpriteDef      Dragon_W3,        $fff6, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE2)
Define_SpriteDef      Dragon_W4,        $fff6, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE1)
Define_SpriteDef      Dragon_W5,        $fff6, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE3)
Define_SpriteDef      Dragon_W6,        $fff6, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharE3)

Define_SpriteDef      Dragon_SW1,       $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE1)
Define_SpriteDef      Dragon_SW2,       $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE2)
Define_SpriteDef      Dragon_SW3,       $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE2)
Define_SpriteDef      Dragon_SW4,       $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE1)
Define_SpriteDef      Dragon_SW5,       $fff0, $ffef, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE3)
Define_SpriteDef      Dragon_SW6,       $fff0, $ffee, SPRITE_LARGE, $00, %01100000, 0
.word .loword(DragonCharSE3)


; ============================================================================
; Animation definitions are below.
; After each animdef, we have an array of sprites to display for each frame,
;  based on which direction the dragon is facing. See the lookup tables below.
; ============================================================================
;                     Name,                   delay, next anim,       callback
Define_AnimDef        Dragon_AnimIdle,        0,     Dragon_AnimIdle, 0
.word .loword(Dragon_S1)
.word .loword(Dragon_SE1)
.word .loword(Dragon_E1)
.word .loword(Dragon_NE1)
.word .loword(Dragon_N1)
.word .loword(Dragon_NW1)
.word .loword(Dragon_W1)
.word .loword(Dragon_SW1)

Define_AnimDef        Dragon_AnimWalk_1,      7,     Dragon_AnimWalk_2,   DragonFootstep1
.word .loword(Dragon_S1)
.word .loword(Dragon_SE1)
.word .loword(Dragon_E1)
.word .loword(Dragon_NE1)
.word .loword(Dragon_N1)
.word .loword(Dragon_NW1)
.word .loword(Dragon_W1)
.word .loword(Dragon_SW1)

Define_AnimDef        Dragon_AnimWalk_2,      5,     Dragon_AnimWalk_3,   0
.word .loword(Dragon_S2)
.word .loword(Dragon_SE2)
.word .loword(Dragon_E2)
.word .loword(Dragon_NE2)
.word .loword(Dragon_N2)
.word .loword(Dragon_NW2)
.word .loword(Dragon_W2)
.word .loword(Dragon_SW2)

Define_AnimDef        Dragon_AnimWalk_3,      5,     Dragon_AnimWalk_4,   0
.word .loword(Dragon_S3)
.word .loword(Dragon_SE3)
.word .loword(Dragon_E3)
.word .loword(Dragon_NE3)
.word .loword(Dragon_N3)
.word .loword(Dragon_NW3)
.word .loword(Dragon_W3)
.word .loword(Dragon_SW3)

Define_AnimDef        Dragon_AnimWalk_4,      7,     Dragon_AnimWalk_5,   DragonFootstep2
.word .loword(Dragon_S4)
.word .loword(Dragon_SE4)
.word .loword(Dragon_E4)
.word .loword(Dragon_NE4)
.word .loword(Dragon_N4)
.word .loword(Dragon_NW4)
.word .loword(Dragon_W4)
.word .loword(Dragon_SW4)

Define_AnimDef        Dragon_AnimWalk_5,      5,     Dragon_AnimWalk_6,   0
.word .loword(Dragon_S5)
.word .loword(Dragon_SE5)
.word .loword(Dragon_E5)
.word .loword(Dragon_NE5)
.word .loword(Dragon_N5)
.word .loword(Dragon_NW5)
.word .loword(Dragon_W5)
.word .loword(Dragon_SW5)

Define_AnimDef        Dragon_AnimWalk_6,      5,     Dragon_AnimWalk_1,   0
.word .loword(Dragon_S6)
.word .loword(Dragon_SE6)
.word .loword(Dragon_E6)
.word .loword(Dragon_NE6)
.word .loword(Dragon_N6)
.word .loword(Dragon_NW6)
.word .loword(Dragon_W6)
.word .loword(Dragon_SW6)


; ============================================================================
; Converts 16-bit D-pad value to direction index. ffff = invalid input, or no input
; 0   2   4   6   8   a   c   e
; S,  SE, E,  NE, N,  NW, W,  SW
; ============================================================================
_DpadToDirectionValue:  ; up | down | left | right
    .word $ffff         ;  0 |    0 |    0 |     0
    .word $0004         ;  0 |    0 |    0 |     1
    .word $000c         ;  0 |    0 |    1 |     0
    .word $ffff         ;  0 |    0 |    1 |     1
    .word $0000         ;  0 |    1 |    0 |     0
    .word $0002         ;  0 |    1 |    0 |     1
    .word $000e         ;  0 |    1 |    1 |     0
    .word $ffff         ;  0 |    1 |    1 |     1
    .word $0008         ;  1 |    0 |    0 |     0
    .word $0006         ;  1 |    0 |    0 |     1
    .word $000a         ;  1 |    0 |    1 |     0
    .word $ffff         ;  1 |    0 |    1 |     1
    .word $ffff         ;  1 |    1 |    0 |     0
    .word $ffff         ;  1 |    1 |    0 |     1
    .word $ffff         ;  1 |    1 |    1 |     0
    .word $ffff         ;  1 |    1 |    1 |     1


; ============================================================================
; Speed constants
; ============================================================================
WALK_SPEED = $200
DIAG_SPEED = $16a


; ============================================================================
; Converts direction index to X and Y velocities
; ============================================================================
_DirectionToVelocity:
            ; X vel,               Y vel,                direction
    .word     0,                   WALK_SPEED          ; S
    .word     DIAG_SPEED,          DIAG_SPEED          ; SE
    .word     WALK_SPEED,          0                   ; E
    .word     DIAG_SPEED,          $10000 - DIAG_SPEED ; NE
    .word     0,                   $10000 - WALK_SPEED ; N
    .word     $10000 - DIAG_SPEED, $10000 - DIAG_SPEED ; NW
    .word     $10000 - WALK_SPEED, 0                   ; W
    .word     $10000 - DIAG_SPEED, DIAG_SPEED          ; SW


; ============================================================================
; Our variable aliases
; ============================================================================
dragonDirection = objCustomA
lastAnimation = objCustomB

SPRITEDEF_CHARDATA = SPRITEDEF_NEXTDEF + 2


; ============================================================================
; These callbacks play footstep noises as the dragon walks.
; ============================================================================
DragonFootstep1:
    lda #SOUND_STEP_1
    sta soundAId
    rts

DragonFootstep2:
    lda #SOUND_STEP_2
    sta soundAId
    rts


; ============================================================================
; Creates the dragon object and initializes the properties
; ============================================================================
SpawnDragon:
    php
    rep #$30
    
    ; Use object pool 0
    ldy #$0000
    ; Try to get an object index from the pool
    jsr CreateObject
    ; If overflow CPU bit is set, we ran out of objects.
    bvs _NotCreated
    ; Otherwise, our X is set to the new object's index. Let's set some variables.
    
    ; Initial position
    sep #$20
    stz objPosSubX, x
    stz objPosSubY, x
    rep #$20
    lda #$0080
    sta objPosX, x
    lda #$0070
    sta objPosY, x
    
    ; Velocity, sprite attribute
    stz objVelX, x
    stz objVelY, x
    stz objAttribute, x
    
    ; Give this object a behavior to run every frame
    lda #.loword(DragonThink)
    sta objThinker, x
    
    ; Give this object an animation.
    ldy #.loword(Dragon_AnimIdle)
    jsr SetAnimation
    
_NotCreated:
    plp
    rts


; ============================================================================
; Thinker function for our dragon. Handles movement and animation.
; ============================================================================
DragonThink:
    ; Get the current directional input
    lda inputRaw
    ; We're only interested in the directions
    and #( INPUT_UP + INPUT_DOWN + INPUT_LEFT + INPUT_RIGHT)
    ; All the directional inputs are in the high byte, so switch to the low byte
    xba
    ; Convert to direction index. Shift left one byte for word-sized index (two bytes).
    asl
    tay
    lda _DpadToDirectionValue, y
    ; If no valid direction, don't change it.
    bmi _NoDirection
    sta dragonDirection, x
    
    ; Since we're holding a direction, set our velocity
    
    ; 4 bytes per table index, so shift left
    asl
    tay
    ; Read X and Y values from the table
    lda _DirectionToVelocity, y
    sta objVelX, x
    lda _DirectionToVelocity + 2, y
    sta objVelY, x
    
    ; Now see if we need to make our dragon walk. Custom value B is the last
    ;  animation we switched to.
    lda lastAnimation, x
    cmp #.loword(Dragon_AnimWalk_1)
    beq _VelocitySet
    
    ; Play the animation
    ldy #.loword(Dragon_AnimWalk_1)
    jsr SetAnimation
    tya
    sta lastAnimation, x
    
    bra _VelocitySet
    
_NoDirection:
    ; Stop in place
    stz objVelX, x
    stz objVelY, x
    
    ; Now see if we need to make our dragon switch to the idle animation.
    lda lastAnimation, x
    cmp #.loword(Dragon_AnimIdle)
    beq _VelocitySet
    
    ; Play the animation
    ldy #.loword(Dragon_AnimIdle)
    jsr SetAnimation
    tya
    sta lastAnimation, x

_VelocitySet:
    
    jsr MoveObject
    
    ; Scroll the view if necessary (only if we happen to be object 0)
    cpx #$0000
    bne _NoScroll
    jsr _ScrollView

_NoScroll:
    
    ; Process any animation
    jsr AnimateWithCallback
    
    ; Our sprite is at the end of the current animdef. Find it and put it in Y.
    lda objAnimDef, x
    ; The end of each animation def has an array of sprites to use based on which
    ;  direction we're facing. So let's offset into that array here.
    clc
    adc dragonDirection
    tay
    ; Here we get the actual spritedef out of the array
    lda ANIMDEF_DATAEX, y
    tay
    
    ; Now let's try to stream this sprite char.
    jsr _StreamSprite
    
    jsr AddSimpleSprite
    
    ; Go to next object (well there's only one object in this example, so...)
    Macro_NextThinker


; ============================================================================
; Streams the dragon's sprite to VRAM.
;
; For this example, we're going to assume that we're only going to be streaming
;  in 32x32 sprites, as four strips of 32x8 data. Additionally, this example
;  does not have a strategy for what to do when running out of DMA time. Since
;  there's only one object with a sprite being streamed, we don't need to worry
;  about that here.
;
; Here we use a convenience function, DMATryAddStrip4.
; ============================================================================
_StreamSprite:
    phx
    
    ; The size of a single strip
    lda #$0080
    ; We want to add four strips of this size
    jsr DMATryAddStrip4
    ; No DMA time, bail out.
    bvs _NoMoreDMA
    
    ; Our source bank
    lda #$80
    sta dmaTableStripSrcBank, x
    ; The char we want to stream to VRAM
    lda SPRITEDEF_CHARDATA, y
    sta dmaTableStripSrc, x
    ; The VRAM address to put it in. If we were streaming for more than one object,
    ;  we'd put different values in here based on which slot the object is assigned.
    ;  For this example, we only have one object, so we'll keep it simple.
    lda #$0000
    sta dmaTableStripDest, x
    
_NoMoreDMA:
    
    plx
    rts


; ============================================================================
; Centers the view on our dragon where appropriate
; ============================================================================
_ScrollView:
    ; Where we would like to move the scroll X position to, if going left.
    ;  Keep in mind that spriteScrollX refers to where the left edge of the screen is!
    lda objPosX, x
    sec
    sbc #$70
    tay
    
    ; If target scroll is to the left of current scroll, then let's scroll there
    sec
    sbc spriteScrollX
    bpl _NoLeftScroll
    sty spriteScrollX
    
    bra _DoYScroll
    
_NoLeftScroll:
    ; Target X scroll, if going right
    lda objPosX, x
    sec
    sbc #$90
    tay
    
    ; If target scroll is to the right of current scroll...
    sec
    sbc spriteScrollX
    bmi _NoRightScroll
    sty spriteScrollX

_NoRightScroll:
_DoYScroll:
    ; Target Y scroll, if going up
    ;  Keep in mind that spriteScrollY refers to where the top edge of the screen is!
    lda objPosY, x
    sec
    sbc #$60
    tay
    
    ; If target scroll is above current scroll...
    sec
    sbc spriteScrollY
    bpl _NoTopScroll
    sty spriteScrollY
    
    bra _ScrollDone
    
_NoTopScroll:
    ; Target Y scroll, if going down
    lda objPosY, x
    sec
    sbc #$80
    tay
    
    ; If target scroll is below of current scroll...
    sec
    sbc spriteScrollY
    bmi _NoBottomScroll
    sty spriteScrollY

_NoBottomScroll:
_ScrollDone:
    
    rts
