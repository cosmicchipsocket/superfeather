
.p816   ; 65816 processor
.smart

.macro Macro_FatalError ERRORID, VAR1, VAR2, VAR3
    rep #$30
    
    lda #$debb
    pha
    lda #$f1ff
    pha
    lda ERRORID
    pha
    lda VAR1
    pha
    lda VAR2
    pha
    lda VAR3
    pha
    
    brk
    nop
.endmacro
