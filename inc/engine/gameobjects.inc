
.p816   ; 65816 processor
.smart

.macro Define_DynSpriteDef NAME, CHARDATA, CHARLENGTH, SPRITEDEF
    NAME:
    .word .loword(CHARDATA)
    .byte <.bank(CHARDATA)
    .word CHARLENGTH / 4
    .word .loword(SPRITEDEF)
.endmacro

.macro Macro_NextThinker
    inx
    inx
    inx
    jmp (objThinker, x)
.endmacro
