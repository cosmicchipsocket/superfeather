
.macro Define_PpuDef NAME, OBJSEL, BGMODE, BG1TILEMAP, BG2TILEMAP, BG3TILEMAP, BG4TILEMAP, BG12CHAR, BG34CHAR, MODE7SEL, WIN12SEL, WIN34SEL, WINOBJSEL, WINBGLOGIC, WINOBJLOGIC, DESTMAIN, DESTSUB, WINMAIN, WINSUB, COLORMATHSEL, COLORMATHDEST, FIXEDCOLOR, SETINI
    NAME:
    .byte OBJSEL ; single
    .byte BGMODE ; single
    .byte BG1TILEMAP, BG2TILEMAP, BG3TILEMAP, BG4TILEMAP, BG12CHAR, BG34CHAR ; single
    .byte MODE7SEL ; single
    .byte WIN12SEL, WIN34SEL, WINOBJSEL ; single
    .byte WINBGLOGIC, WINOBJLOGIC ; single
    .byte DESTMAIN, DESTSUB, WINMAIN, WINSUB ; single
    .byte COLORMATHSEL, COLORMATHDEST ; single
    .word FIXEDCOLOR ; special
    .byte SETINI ; single
.endmacro

.macro Define_SpriteDef NAME, XOFFSET, YOFFSET, SPRITESIZE, TILE, FLAGS, NEXTSPRITE
    NAME:
    .word XOFFSET
    .word YOFFSET
    .if SPRITESIZE = SPRITE_SMALL
        .byte $00
    .elseif SPRITESIZE = SPRITE_LARGE
        .byte $02
    .else
        .error "Sprite size must be SPRITE_SMALL or SPRITE_LARGE."
    .endif
    .byte TILE
    .byte FLAGS
    .word .loword(NEXTSPRITE)
.endmacro

.macro Define_AnimDef NAME, DELAY, NEXTANIM, DATAPOINTER
    NAME:
    .byte DELAY
    .word .loword(NEXTANIM)
    .word .loword(DATAPOINTER)
.endmacro
