local app = require "src.app"
local test = require "src.utils.test"

local DropTargetScreen = require "src.screens.droptargetscreen"

local PaletteScreen = require "src.screens.editors.palettescreen"
local CharScreen = require "src.screens.charscreen"
local TilemapScreen = require "src.screens.editors.tilemapscreen"
local SpriteSlicerScreen = require "src.screens.editors.spriteslicerscreen"

local NewRawScreen = require "src.screens.newrawscreen"

local MainScreen = DropTargetScreen:makeClass
{
    message = "Drag a file or folder onto this window to edit it.",
    message2 = "Supported filetypes:",
    message3 = "Supported foldertypes:",
    message4 = "Or press N to create a blank raw file.",
    timer = 0
}

MainScreen.filetypeMapping =
{
    {
        extension = ".pal",
        description = "Palette",
        purpose = "View and edit raw SNES palette files",
        handler = function(self, file)
            test.unusedArgs(self)
            app.setScreen(PaletteScreen:new(file))
        end
    },
    {
        extension = ".pic",
        description = "Char GFX",
        purpose = "View raw 4bpp SNES char data",
        handler = function(self, file)
            test.unusedArgs(self)
            app.setScreen(CharScreen:new(file))
        end
    },
    {
        extension = ".pic2",
        description = "BG3 char GFX",
        purpose = "View raw 2bpp SNES char data",
        handler = function(self, file)
            test.unusedArgs(self)
            app.setScreen(CharScreen:new(file))
        end
    },
    {
        extension = ".map",
        description = "Tilemap",
        purpose = "View and edit raw SNES tilemaps",
        handler = function(self, file)
            test.unusedArgs(self)
            app.setScreen(TilemapScreen:new(file))
        end
    },
}

MainScreen.foldertypeMapping =
{
    {
        extension = ".dynsprite",
        description = "Dynamic sprite",
        purpose = "Use the Sprite Slicer to generate streamable sprite graphics from a spritesheet",
        handler = function(self, path)
            test.unusedArgs(self)
            app.setScreen(SpriteSlicerScreen:new(path))
        end
    },
}

MainScreen.keyHandlers =
{
    n = function()
        app.setScreen(NewRawScreen:new())
    end
}

function MainScreen:badFileDrop(file)
    test.unusedArgs(file)
    self.message = "Unknown file extension. Please try a different file."
end

function MainScreen:badFolderDrop(path)
    test.unusedArgs(path)
    self.message = "Unknown folder extension. Please try a different folder."
end

function MainScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(scancode)
    local func = self.keyHandlers[key]

    if func then
        func(self, isRepeat)
    end
end

function MainScreen:init(message)
    DropTargetScreen.init(self)
    
    if message then
        self.message = message
    end
    
    self.animating = true
end

function MainScreen:update(dt)
    -- Workaround for desktop effects switch giving blank window - force updates for a couple seconds
    if self.timer < 2 then
        self.timer = self.timer + dt
        self.animating = true
        self.dirty = true
    else
        self.animating = false
    end
end

function MainScreen:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(app.res.uifont)
    
    local prompt = self.message .. "\n\n\n" .. self.message2 .. "\n\n"
    
    for _, mapping in ipairs(self.filetypeMapping) do
        prompt = prompt .. "                [" .. mapping.extension .. "]: " .. mapping.description .. "\n"
                        .. "                                " .. mapping.purpose .. "\n"
    end
    
    prompt = prompt .. "\n" .. self.message3 .. "\n\n"
    
    for _, mapping in ipairs(self.foldertypeMapping) do
        prompt = prompt .. "                [" .. mapping.extension .. "]: " .. mapping.description .. "\n"
                        .. "                                " .. mapping.purpose .. "\n"
    end
    
    prompt = prompt .. "\n\n" .. self.message4
    
    love.graphics.print(prompt, 32, 32)
end

return MainScreen
