local app = require "src.app"

local EditorScreen = require "src.screens.editors.editorscreen"
local NameEntryScreen = require "src.screens.nameentryscreen"

local helpString = "Shift + Arrow keys - Reorder"
                    .. "\nCtrl + Ins/Del - Add/delete metasprite"
                    .. "\nR - Rename metasprite"
                    .. "\n\nEsc - Back"

local SpriteManagerScreen = EditorScreen:makeClass
{
    handlers =
    {
        keypressed = EditorScreen.handlers.keypressed,
        mousepressed = EditorScreen.handlers.mousepressed,
        mousereleased = EditorScreen.handlers.mousereleased,
        mousemoved = EditorScreen.handlers.mousemoved,
        wheelmoved = EditorScreen.handlers.wheelmoved,
    }
}

SpriteManagerScreen.keyHandlers =
{
    left = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self.spriteSlicerScreen:reorderSpriteBack()
        else
            self.spriteSlicerScreen:selectPreviousSprite()
        end
        self.dirty = true
    end,
    right = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self.spriteSlicerScreen:reorderSpriteForward()
        else
            self.spriteSlicerScreen:selectNextSprite()
        end
        self.dirty = true
    end,
    up = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self.spriteSlicerScreen:reorderMetaspriteBack()
        else
            self.spriteSlicerScreen:selectPreviousMetasprite()
        end
        self.dirty = true
    end,
    down = function(self, isRepeat)
        if love.keyboard.isDown("lshift", "rshift") then
            self.spriteSlicerScreen:reorderMetaspriteForward()
        else
            self.spriteSlicerScreen:selectNextMetasprite()
        end
        self.dirty = true
    end,
    insert = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self.spriteSlicerScreen:insertMetasprite()
        end
        self.dirty = true
    end,
    delete = function(self, isRepeat)
        if love.keyboard.isDown("lctrl", "rctrl") then
            self.spriteSlicerScreen:deleteMetasprite()
        end
        self.dirty = true
    end,
    r = function(self, isRepeat)
        local parent = self.spriteSlicerScreen

        if not parent then return end

        local index = parent.currentMetasprite
        local metasprite = parent.project.data.metasprites[index]

        if not metasprite then return end
        
        app.pushScreen(NameEntryScreen:new(
            function(newName)
                self.spriteSlicerScreen:renameMetasprite(newName)
            end
        , metasprite.name))
    end,
}

function SpriteManagerScreen:init(spriteSlicerScreen)
    if not spriteSlicerScreen then
        error("Where's the sprite slicer screen?")
    end
    
    EditorScreen.init(self)
    
    self.spriteSlicerScreen = spriteSlicerScreen
end

function SpriteManagerScreen:undo()
    self.spriteSlicerScreen:undo()
    self.dirty = true
end

function SpriteManagerScreen:redo()
    self.spriteSlicerScreen:redo()
    self.dirty = true
end

function SpriteManagerScreen:drawContent(width, height)
    self.spriteSlicerScreen:drawContent(width, height)
end

function SpriteManagerScreen:drawSidebar(width, height)
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(app.res.uifontLarge)
    love.graphics.print("Sprite Manager", 16, 16)
    
    local parent = self.spriteSlicerScreen
    local metaspriteCount = #parent.project.data.metasprites
    
    local metaspriteString = parent.currentMetasprite .. " of " .. metaspriteCount
    
    love.graphics.setFont(app.res.uifont)
    
    love.graphics.print("Metasprite", 16, 80)
    love.graphics.print(metaspriteString, 128, 80)
    
    love.graphics.setFont(app.res.uifontSmall)
    love.graphics.print("Up/Down - Switch metasprite", 16, 100)
    
    love.graphics.setFont(app.res.uifont)
    
    local listSize = math.floor((height - 370) / 20)
    local listOffset = math.max(0, math.min(metaspriteCount - listSize, parent.currentMetasprite - math.ceil((listSize - 0.5) / 2)))
    
    for i = 1, listSize do
        local index = i + listOffset
        local metasprite = parent.project.data.metasprites[index]
        
        if metasprite then
            local y = 116 + i * 20
            
            if index == parent.currentMetasprite then
                love.graphics.rectangle("fill", 12, y, width, 20)
                love.graphics.setColor(0, 0, 0)
                love.graphics.print(tostring(metasprite.name), 16, y)
                love.graphics.setColor(255, 255, 255)
            else
                love.graphics.print(tostring(metasprite.name), 16, y)
            end
        end
    end
    
    local metasprite = parent.project.data.metasprites[parent.currentMetasprite]
    local numSprites = metasprite and metasprite.sprites and #metasprite.sprites or 0
    local spriteString = parent.currentSprite .. " of " .. numSprites
    local helpTextY = height - (9*16)
    
    love.graphics.setFont(app.res.uifont)
    
    love.graphics.print("HW Sprite", 16, helpTextY - 68)
    love.graphics.print(spriteString, 128, helpTextY - 68)
    
    love.graphics.setFont(app.res.uifontSmall)
    love.graphics.print("Left/Right - Switch HW sprite", 16, helpTextY - 48)
    
    if height >= 480 then
        love.graphics.print(helpString, 16, helpTextY)
    end
end

return SpriteManagerScreen
