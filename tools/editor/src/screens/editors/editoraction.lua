local oops = require "src.utils.oops"
local test = require "src.utils.test"

local EditorAction = oops.BaseObject:makeClass
{
    
}

function EditorAction:init()
    test.unusedArgs(self)
end

function EditorAction:undo()
    test.unusedArgs(self)
    test.implementMe()
end

function EditorAction:redo()
    test.unusedArgs(self)
    test.implementMe()
end

function EditorAction:isChange()
    test.unusedArgs(self)
    test.implementMe()
end

return EditorAction
