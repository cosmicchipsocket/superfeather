local app = require "src.app"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local EditorScreen = require "src.screens.editors.editorscreen"
local EditorAction = require "src.screens.editors.editoraction"
local PaletteData = require "src.dataobjs.palettedata"
local SavePromptScreen = require "src.screens.savepromptscreen"

local helpString = "S - Save"
                   .. "\nCtrl+C/Ctrl+V - Copy/paste"
                   .. "\nArrows/left click - Select color"
                   .. "\nR/F - Adjust Red"
                   .. "\nT/G - Adjust Green"
                   .. "\nY/H - Adjust Blue"
                   .. "\n-/= - Zoom (adjust columns)"


local ChangeColorAction = EditorAction:makeClass
{
    oldRed = 0,
    oldGreen = 0,
    oldBlue = 0,
    newRed = 0,
    newGreen = 0,
    newBlue = 0
}

function ChangeColorAction:init(paletteScreen, colorIndex, newRed, newGreen, newBlue)
    self.paletteScreen = paletteScreen
    self.colorIndex = colorIndex
    
    local color = self.paletteScreen.paletteData.colors[self.colorIndex]
    
    self.oldRed = color.red
    self.oldGreen = color.green
    self.oldBlue = color.blue
    self.newRed = extramath.clampValue(0, 31, newRed)
    self.newGreen = extramath.clampValue(0, 31, newGreen)
    self.newBlue = extramath.clampValue(0, 31, newBlue)
end

function ChangeColorAction:undo()
    local color = self.paletteScreen.paletteData.colors[self.colorIndex]
    
    color.red = self.oldRed
    color.green = self.oldGreen
    color.blue = self.oldBlue
    
    self.paletteScreen:setSelectedColor(self.colorIndex)
end

function ChangeColorAction:redo()
    local color = self.paletteScreen.paletteData.colors[self.colorIndex]
    
    color.red = self.newRed
    color.green = self.newGreen
    color.blue = self.newBlue
    
    self.paletteScreen:setSelectedColor(self.colorIndex)
end

function ChangeColorAction:isChange()
    return self.oldRed ~= self.newRed or 
           self.oldGreen ~= self.newGreen or 
           self.oldBlue ~= self.newBlue
end


local PaletteScreen = EditorScreen:makeClass
{
    handlers =
    {
        keypressed = EditorScreen.handlers.keypressed,
        filedropped = EditorScreen.handlers.filedropped,
        mousepressed = EditorScreen.handlers.mousepressed,
        mousereleased = EditorScreen.handlers.mousereleased,
        mousemoved = EditorScreen.handlers.mousemoved,
        wheelmoved = EditorScreen.handlers.wheelmoved,
    },
    rowSize = 16,
    rowMin = 4,
    rowMax = 64,
    clipboardId = "Palette"
}

PaletteScreen.filetypeMapping =
{
    {
        extension = ".pal",
        description = "SNES palette",
        handler = function(self, file)
            self:newPalette(file)
        end
    }
}

PaletteScreen.keyHandlers =
{
    s = function(self, isRepeat)
        if not isRepeat then
            self.paletteData:saveToFile()
            self.unsaved = false
        end
    end,
    left = function(self)
        self:setSelectedColor(self.selectedColor - 1)
        self.needScroll = true
    end,
    right = function(self)
        self:setSelectedColor(self.selectedColor + 1)
        self.needScroll = true
    end,
    up = function(self)
        self:setSelectedColor(self.selectedColor - self.rowSize)
        self.needScroll = true
    end,
    down = function(self)
        self:setSelectedColor(self.selectedColor + self.rowSize)
        self.needScroll = true
    end,
    r = function(self)
        self:adjustColor(self.selectedColor, 1, 0, 0)
    end,
    f = function(self)
        self:adjustColor(self.selectedColor, -1, 0, 0)
    end,
    t = function(self)
        self:adjustColor(self.selectedColor, 0, 1, 0)
    end,
    g = function(self)
        self:adjustColor(self.selectedColor, 0, -1, 0)
    end,
    y = function(self)
        self:adjustColor(self.selectedColor, 0, 0, 1)
    end,
    h = function(self)
        self:adjustColor(self.selectedColor, 0, 0, -1)
    end,
    ["-"] = function(self)
        self:zoomOut()
    end,
    ["="] = function(self)
        self:zoomIn()
    end,
}

function PaletteScreen:mousePressedContent(x, y, button, w, h)
    if button == 1 then
        self:mouseSelectColor(x, y, w, h)
    end
end

function PaletteScreen:mouseMovedContent(x, y, w, h)
    if love.mouse.isDown(1) then
        self:mouseSelectColor(x, y, w, h)
    end
end

function PaletteScreen:mouseSelectColor(x, y, w, h)
    test.unusedArgs(h)
    local displaySize = math.floor(w / self.rowSize)
    local gridX = math.floor(x / displaySize)
    local gridY = math.floor(y / displaySize)
    local offset = extramath.round(self.contentScroll) * self.rowSize
    
    if gridX >= 0 and gridX < self.rowSize then
        local index = gridX + (gridY * self.rowSize) + 1 + offset
        
        if index >= 1 and index <= #self.paletteData.colors then
            self:setSelectedColor(index)
        end
    end
end

function PaletteScreen:setSelectedColor(index)
    self.selectedColor = extramath.clampValue(1, #self.paletteData.colors, index)
    self.dirty = true
end

function PaletteScreen:zoomIn()
    self.rowSize = extramath.clampValue(self.rowMin, self.rowMax, self.rowSize / 2)
    self.dirty = true
end

function PaletteScreen:zoomOut()
    self.rowSize = extramath.clampValue(self.rowMin, self.rowMax, self.rowSize * 2)
    self.dirty = true
end

function PaletteScreen:colorIndexToCoords(index, displaySize)
    local colorX = ((index - 1) % self.rowSize) * displaySize
    local colorY = math.floor((index - 1) / self.rowSize) * displaySize
    
    return colorX, colorY
end

function PaletteScreen:adjustColor(index, offsetRed, offsetGreen, offsetBlue)
    local color = self.paletteData.colors[index]
    
    if not color then
        error("Bad color index")
    end
    
    self:undoCheckpoint()
    self:doEditAction(ChangeColorAction:new(self, index, color.red + offsetRed,
                                            color.green + offsetGreen, color.blue + offsetBlue))
end

function PaletteScreen:copyToClipboard()
    local color = self.paletteData.colors[self.selectedColor]
    return tostring(PaletteData.rgbToWord(color))
end

function PaletteScreen:pasteFromClipboard(clipboardData)
    local colorWord = extramath.clampValue(0, 32767, math.floor(tonumber(clipboardData)))
    
    local newColor = PaletteData.wordToRgb(colorWord)
    
    self:undoCheckpoint()
    self:doEditAction(ChangeColorAction:new(self, self.selectedColor,
                                            newColor.red, newColor.green, newColor.blue))
end

function PaletteScreen:newPalette(filepath)
    local loadFunc = function()
        self.paletteData = PaletteData:new(filepath)
        self:setSelectedColor(self.selectedColor)
        
        self.title = filepath
        self:setTitle()
        
        self.dirty = true
        self.unsaved = false
    end
    
    if self.unsaved then
        app.pushScreen(SavePromptScreen:new("load", loadFunc))
    else
        loadFunc()
    end
end

function PaletteScreen:init(filepath)
    EditorScreen.init(self)

    self.selectedColor = 1
    self:newPalette(filepath)
end

function PaletteScreen:drawContent(width, height)
    local displaySize = math.floor(width / self.rowSize)
    local totalLines = math.ceil(#self.paletteData.colors / self.rowSize)
    local scrollLines = math.max(0, totalLines - math.floor(height / displaySize))
    local visibleLines = totalLines - scrollLines + 1
    
    -- Update scroll if keyboard control used
    if self.needScroll then
        local maxLine = math.floor((self.selectedColor - 1) / self.rowSize)
        local minLine = math.max(0, maxLine - visibleLines + 2)
        
        self.contentScroll = extramath.clampValue(minLine, maxLine, self.contentScroll)
        self.needScroll = false
    end
    
    self:updateScrollInfo(0, scrollLines, 1,  scrollLines > 0 and 1 - (scrollLines / totalLines) or 1)
    
    local offset = extramath.round(self.contentScroll) * self.rowSize
    
    for i = offset + 1, offset + (visibleLines * self.rowSize) do
        local color = self.paletteData.colors[i]
        
        if color then
            local colorX, colorY = self:colorIndexToCoords(i - offset, displaySize)
            
            love.graphics.setColor(color.red / 31 * 255, color.green / 31 * 255, color.blue / 31 * 255)
            love.graphics.rectangle("fill", colorX, colorY, displaySize, displaySize)
        end
    end
    
    local colorX, colorY = self:colorIndexToCoords(self.selectedColor - offset, displaySize)
    local color = self.paletteData.colors[self.selectedColor]
    local averageSelected = (color.red + color.green + color.blue) / 3
    
    if averageSelected > 20 then
        love.graphics.setColor(0, 0, 0)
    else
        love.graphics.setColor(255, 255, 255)
    end
    
    love.graphics.setLineWidth(2)
    love.graphics.setLineStyle("smooth")
    love.graphics.setScissor()
    love.graphics.rectangle("line", colorX, colorY, displaySize, displaySize)
end

function PaletteScreen:drawSidebar(width, height)
    test.unusedArgs(width)
    love.graphics.setFont(app.res.uifont)
    love.graphics.setColor(255, 255, 255)
    
    local realIndex = self.selectedColor - 1
    local rowIndex = math.floor(realIndex / 16)
    local colIndex = realIndex % 16
    
    love.graphics.print("Color " .. realIndex .. "\t\t(row " .. rowIndex .. ", col " .. colIndex .. ")", 16, 16)
    
    local color = self.paletteData.colors[self.selectedColor]
    local hue, sat, light = self.paletteData:getHsl(self.selectedColor)
    
    love.graphics.print("Red\nGreen\nBlue\n\nHue\nSat\nLight", 16, 184)
    love.graphics.print("" .. color.red
                        .. "\n" .. color.green
                        .. "\n" .. color.blue
                        .. "\n\n" .. extramath.round(hue) .. "°"
                        .. "\n" .. extramath.round(sat) .. "%"
                        .. "\n" .. extramath.round(light) .. "%", 96, 184)
    
    love.graphics.setColor(color.red / 31 * 255, color.green / 31 * 255, color.blue / 31 * 255)
    love.graphics.rectangle("fill", 16, 40, 208, 128)
    
    love.graphics.setColor(248, 32, 80)
    love.graphics.rectangle("fill", 128, 184, color.red * 3, 16)
    love.graphics.setColor(32, 248, 80)
    love.graphics.rectangle("fill", 128, 204, color.green * 3, 16)
    love.graphics.setColor(32, 80, 248)
    love.graphics.rectangle("fill", 128, 224, color.blue * 3, 16)
    
    love.graphics.setFont(app.res.uifontSmall)
    love.graphics.setColor(255, 255, 255)
    if height >= 480 then
        love.graphics.print(helpString, 16, height - (7*16) - 16)
    end
end

return PaletteScreen
