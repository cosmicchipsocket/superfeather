local app = require "src.app"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local EditorScreen = require "src.screens.editors.editorscreen"

local maxTiles = 1024

local helpString = "Arrows/Left click - Select tile"
                   .. "\nEnter/Click again - Confirm"
                   .. "\n0-7 - Set palette"
                   .. "\nH, V - Toggle H/V flip"
                   .. "\nP - Toggle priority"

local TilePickerScreen = EditorScreen:makeClass
{
    handlers =
    {
        keypressed = EditorScreen.handlers.keypressed,
        mousepressed = EditorScreen.handlers.mousepressed,
        mousereleased = EditorScreen.handlers.mousereleased,
        mousemoved = EditorScreen.handlers.mousemoved,
        wheelmoved = EditorScreen.handlers.wheelmoved,
    },
    columns = 16,
    clipboardId = "TilePicker"
}

TilePickerScreen.keyHandlers =
{
    left = function(self)
        self:setSelectedTile(self.pickerData.index - 1)
        self.needScroll = true
    end,
    right = function(self)
        self:setSelectedTile(self.pickerData.index + 1)
        self.needScroll = true
    end,
    up = function(self)
        self:setSelectedTile(self.pickerData.index - self.columns)
        self.needScroll = true
    end,
    down = function(self)
        self:setSelectedTile(self.pickerData.index + self.columns)
        self.needScroll = true
    end,
    ["`"] = function(self)
        self:setPalette(0)
    end,
    ["0"] = function(self)
        self:setPalette(0)
    end,
    ["1"] = function(self)
        self:setPalette(1)
    end,
    ["2"] = function(self)
        self:setPalette(2)
    end,
    ["3"] = function(self)
        self:setPalette(3)
    end,
    ["4"] = function(self)
        self:setPalette(4)
    end,
    ["5"] = function(self)
        self:setPalette(5)
    end,
    ["6"] = function(self)
        self:setPalette(6)
    end,
    ["7"] = function(self)
        self:setPalette(7)
    end,
    h = function(self)
        self.pickerData.hflip = not self.pickerData.hflip
        self.dirty = true
    end,
    v = function(self)
        self.pickerData.vflip = not self.pickerData.vflip
        self.dirty = true
    end,
    p = function(self)
        self.pickerData.priority = not self.pickerData.priority
        self.dirty = true
    end,
    ["return"] = function(self)
        test.unusedArgs(self)
        app.popScreen()
    end,
}

function TilePickerScreen:mousePressedContent(x, y, button, w, h)
    if button == 1 then
        self.changedTile = false
        self:mouseSelectTile(x, y, w, h)
    end
end

function TilePickerScreen:mouseMovedContent(x, y, w, h)
    if love.mouse.isDown(1) then
        self:mouseSelectTile(x, y, w, h)
    end
end

function TilePickerScreen:mouseReleasedContent(x, y, button, w, h)
    test.unusedArgs(x, y, w, h)
    
    if button == 1 and not self.changedTile and not self.popped then
        self.popped = true
        app.popScreen()
    end
end

function TilePickerScreen:screenToGridCoords(x, y, w)
    local scale = self:getTileScale(w)
    return math.floor(x / 8 / scale), math.floor((y + self.contentScroll) / 8 / scale)
end

function TilePickerScreen:mouseSelectTile(x, y, w, h)
    test.unusedArgs(h)
    
    local tileX, tileY = self:screenToGridCoords(x, y, w)
    local index = tileX + tileY * self.columns
    
    local oldIndex = self.pickerData.index
    self:setSelectedTile(index)
    
    self.changedTile = self.changedTile or (oldIndex ~= self.pickerData.index)
end

function TilePickerScreen:setSelectedTile(index)
    self.pickerData.index = extramath.clampValue(0, #self.quads[0], index)
    self.dirty = true
end

function TilePickerScreen:setPalette(palette)
    self.pickerData.palette = palette % 8
    self.dirty = true
end

function TilePickerScreen:getTileScale(width)
    local scale = width / (self.texture:getWidth() / 8)
    
    if scale >= 1 then scale = math.floor(scale) end
    
    return scale
end

function TilePickerScreen:init(texture, quads, pickerData)
    EditorScreen.init(self)
    
    if not texture or not quads or not pickerData then error("Where's the texture, quads, and picker data??") end
    
    self.texture = texture
    self.quads = quads
    self.pickerData = pickerData
    
    -- Scroll to bottom at first, then scroll back up on first draw
    self.contentScroll = math.huge
    self.needScroll = true
    
    self.batches = {}
    
    for i = 0, #quads do
        local quad = quads[i]
        self.batches[i] = love.graphics.newSpriteBatch(self.texture, maxTiles, "static")
        
        for j = 0, #quad do
            self.batches[i]:add(quad[j], (j % self.columns) * 8, math.floor(j / self.columns) * 8)
        end
    end
end

function TilePickerScreen:drawContent(width, height)
    local index = self.pickerData.index
    local palette = self.pickerData.palette % 8
    
    if not self.batches or not self.batches[palette] then
        return
    end
    
    local scale = self:getTileScale(width)
    local textureHeight = self.texture:getHeight() * scale
    local scrollSize = math.max(0, textureHeight - height)
    
    local tileX = (index % self.columns) * 8 * scale
    local tileY = math.floor(index / self.columns) * 8 * scale
    
    -- Update scroll if keyboard control used
    if self.needScroll then
        local maxY = tileY
        local minY = math.max(0, (tileY + 8 * scale) - height)
        
        self.contentScroll = extramath.clampValue(minY, maxY, self.contentScroll)
        self.needScroll = false
    end
    
    self:updateScrollInfo(0, scrollSize, 32, scrollSize > 0 and height / textureHeight or 1)
    
    love.graphics.push()
    love.graphics.translate(0, -self.contentScroll)
    
    love.graphics.draw(self.batches[palette], 0, 0, 0, scale, scale)
    
    -- Draw selection rectangle
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setLineWidth(2)
    love.graphics.setLineStyle("smooth")
    love.graphics.setScissor()
    love.graphics.rectangle("line", tileX, tileY, scale * 8, scale * 8)
    
    love.graphics.pop()
end

function TilePickerScreen:drawSidebarTile(x, y)
    if not self.quads then return end
    
    local function yesNo(boolean)
        return boolean and "Yes" or "No"
    end
    
    local function highLow(boolean)
        return boolean and "High" or "Low"
    end
    
    local quad = self.quads[self.pickerData.palette]
    local w = self.pickerData.hflip and -1 or 1
    local h = self.pickerData.vflip and -1 or 1
    
    if quad and quad[self.pickerData.index] then
        love.graphics.draw(self.texture, quad[self.pickerData.index], x + 64, y + 64, 0, 16 * w, 16 * h, 4, 4)
    end
    
    love.graphics.setFont(app.res.uifont)
    love.graphics.print("Char index\nPalette\nH-flip\nV-flip\nPriority", x, y + 144)
    love.graphics.print("" .. self.pickerData.index
                        .. "\n" .. self.pickerData.palette
                        .. "\n" .. yesNo(self.pickerData.hflip)
                        .. "\n" .. yesNo(self.pickerData.vflip)
                        .. "\n" .. highLow(self.pickerData.priority), x + 100, y + 144)
end

function TilePickerScreen:drawSidebar(width, height)
    test.unusedArgs(width)
    if not self.pickerData then return end
    
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(app.res.uifontLarge)
    love.graphics.print("Tile Picker", 16, 16)
    
    self:drawSidebarTile(16, 80)
    
    love.graphics.setFont(app.res.uifontSmall)
    if height >= 450 then
        love.graphics.print(helpString, 16, height - (5*16) - 16)
    end
end

return TilePickerScreen
