
local utf8 = require("utf8")

local app = require "src.app"
local test = require "src.utils.test"

local Screen = require "src.screens.screen"

local NameEntryScreen = Screen:makeClass
{
    handlers = {},
    name = ""
}

function NameEntryScreen:init(callback, initialName)
    Screen.init(self)
    
    if not callback or type(callback) ~= "function" then
        error("Callback function wasn't supplied!")
    end
    
    self.callback = callback
    self.name = initialName or ""
end

function NameEntryScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(scancode, isRepeat)
    
    if key == "backspace" then
        local offset = utf8.offset(self.name, -1)
        
        if offset then
            self.name = self.name:sub(1, offset - 1)
            self.dirty = true
        end
    elseif key == "return" and self.callback then
        app.popScreen()
        self.callback(self.name)
    end
end

function NameEntryScreen.handlers.textinput(self, text)
    self.name = self.name .. text
    self.dirty = true
end

function NameEntryScreen:draw()
    love.graphics.setColor(255, 255, 255)
    
    love.graphics.setFont(app.res.uifont)
    love.graphics.print("Please choose a new name and press Enter:", 32, 32)
    love.graphics.setFont(app.res.uifontLarge)
    
    local width = love.graphics.getDimensions()
    love.graphics.printf(self.name, 32, 64, width - 64)
end

return NameEntryScreen
