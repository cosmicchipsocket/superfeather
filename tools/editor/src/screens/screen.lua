
local oops = require "src.utils.oops"
local test = require "src.utils.test"

local Screen = oops.BaseObject:makeClass
{
    bgRed = 32,
    bgGreen = 32,
    bgBlue = 32,
    animating = false,
    dirty = true,
    handlers = {}
}

function Screen:setTitle()
    if self.title then
        love.window.setTitle("Heartcore Editor :: " .. self.title)
    else
        love.window.setTitle("Heartcore Editor")
    end
end

function Screen:init()
    self:setTitle()
end

function Screen:update(dt)
    test.unusedArgs(self, dt)
    
    return true
end

function Screen:draw()
    test.unusedArgs(self)
end

return Screen
