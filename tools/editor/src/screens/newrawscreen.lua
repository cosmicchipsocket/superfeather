
local bit = require "bit"

local app = require "src.app"
local cabin = require "src.utils.cabin"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local Screen = require "src.screens.screen"
local MessageScreen = require "src.screens.messagescreen"
local NameEntryScreen = require "src.screens.nameentryscreen"

local NewRawScreen = Screen:makeClass
{
    handlers = {}
}

NewRawScreen.keyHandlers =
{
    up = function(self)
        self:adjustDigits(16)
    end,
    down = function(self)
        self:adjustDigits(-16)
    end,
    ["0"] = function(self)
        self:typeDigit("0")
    end,
    ["1"] = function(self)
        self:typeDigit("1")
    end,
    ["2"] = function(self)
        self:typeDigit("2")
    end,
    ["3"] = function(self)
        self:typeDigit("3")
    end,
    ["4"] = function(self)
        self:typeDigit("4")
    end,
    ["5"] = function(self)
        self:typeDigit("5")
    end,
    ["6"] = function(self)
        self:typeDigit("6")
    end,
    ["7"] = function(self)
        self:typeDigit("7")
    end,
    ["8"] = function(self)
        self:typeDigit("8")
    end,
    ["9"] = function(self)
        self:typeDigit("9")
    end,
    backspace = function(self)
        self:eraseDigit()
    end,
    ["return"] = function(self)
        app.pushScreen(NameEntryScreen:new(
            function(filepath)
                cabin.info("Writing file: " .. filepath)
                
                local file, message = io.open(filepath, "wb")
                
                if not file then
                    app.pushScreen(MessageScreen:new("Failed to open file: " .. message))
                    return
                end
                
                local fileData = ""
                
                for _ = 1, self.byteSize do
                    fileData = fileData .. string.char(0)
                end
                
                local success, writeMessage = file:write(fileData)
                
                if not success then
                    app.pushScreen(MessageScreen:new("Failed to write to file: " .. writeMessage))
                    return
                end
                
                file:close()
                
                app.popScreen()
            end
        , "../../data/newfile.bin"))
    end,
}

function NewRawScreen:init()
    Screen.init(self)
    
    self.byteSize = 0
end

function NewRawScreen:typeDigit(digitString)
    local byteString = tostring(self.byteSize)
    byteString = byteString .. digitString
    
    local newByteSize = tonumber(byteString)

    if newByteSize then
        self.byteSize = extramath.clampValue(0, 65536, math.floor(newByteSize))
        self.dirty = true
    end
end

function NewRawScreen:eraseDigit()
    local byteString = tostring(self.byteSize)
    byteString = byteString:sub(1, -2)
    
    local newByteSize = tonumber(byteString)

    if newByteSize then
        self.byteSize = extramath.clampValue(0, 65536, math.floor(newByteSize))
    else
        self.byteSize = 0
    end
    
    self.dirty = true
end

function NewRawScreen:adjustDigits(offset)
    self.byteSize = extramath.clampValue(0, 65536, math.floor(self.byteSize + offset))
    
    self.dirty = true
end

function NewRawScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(scancode)
    local func = self.keyHandlers[key]

    if func then
        func(self, isRepeat)
    end
end

function NewRawScreen:draw()
    love.graphics.setColor(255, 255, 255)
    
    love.graphics.setFont(app.res.uifont)
    love.graphics.print("Please enter the byte size of the new file and press Enter.", 32, 32)
    love.graphics.setFont(app.res.uifontSmall)
    love.graphics.print("(Type value, or use arrow keys to adjust by 16)", 32, 52)
    
    love.graphics.setFont(app.res.uifontLarge)
    love.graphics.printf(self.byteSize .. " bytes", 32, 80, 384, "right")
    
    love.graphics.setFont(app.res.uifont)
    
    love.graphics.printf("0x" .. bit.tohex(self.byteSize, self.byteSize == 65536 and 5 or 4) .. " bytes",
                         32, 120, 384, "right")
    love.graphics.printf("0x" .. bit.tohex(self.byteSize / 2, 4) .. " words",
                         32, 140, 384, "right")
    
    local prompt = "Size suggestions:\n\n" ..
            "                512 bytes  =  Full 256-color palette\n" ..
            "                2048 bytes  =  32x32 tilemap\n" ..
            "                4096 bytes  =  64x32 or 32x64 tilemap\n" ..
            "                8192 bytes  =  64x64 tilemap\n"
    
    love.graphics.print(prompt, 32, 176)
end

return NewRawScreen
