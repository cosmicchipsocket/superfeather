--[[
See LICENSE for important information on distribution.
--]]

local cabin = require "src.utils.cabin"

local app =
{
    running = true,
    screenStack = {}
}

local function _standardOut(message, severity)
    local function writeLine(line)
        io.stdout:write("[" .. severity .. "] " .. line .. "\n")
    end

    message = message .. "\n"

    message:gsub("(.-)[\r\n]", writeLine)
end

local function _handleEvent(e, a, b, c, d)
    if not e then
        return
    end

    if e == "quit" then
        if app.promptScreenClass and app.hasUnsavedChanges("quit") then
            if not app.screen:instanceOf(app.promptScreenClass) then
                app.pushScreen(app.promptScreenClass:new("quit", app.quit))
            else
                app.popScreen()
            end
        else
            app.quit()
        end
    end
    if e == "keypressed" and a == "escape" then
        if app.promptScreenClass and app.hasUnsavedChanges("pop")
                and not app.screen:instanceOf(app.promptScreenClass) then
            app.pushScreen(app.promptScreenClass:new("pop", app.popScreen))
        else
            app.popScreen()
        end
    end

    love.handlers[e](a, b, c, d)

    if app.screen and app.screen.handlers and app.screen.handlers[e] then
        app.screen.handlers[e](app.screen, a, b, c, d)
    end

    if app.screen and e == "resize" or e == "visible" or e == "focus" then
        app.screen.dirty = true
    end
end

function app.quit()
    if not love.quit or not love.quit() then
        if love.audio then
            love.audio.stop()
        end
        app.running = false
        return
    end
end

function app.setScreen(newScreen)
    app.newScreen = newScreen
end

function app.pushScreen(newScreen)
    table.insert(app.screenStack, app.screen)
    app.newScreen = newScreen
end

function app.popScreen()
    local oldScreen = table.remove(app.screenStack)
    
    if oldScreen then
        oldScreen:setTitle()
        oldScreen.dirty = true
        app.newScreen = oldScreen
    else
        app.goHome()
    end
end

function app.setHome(screenClass)
    app.screenStack = {}
    app.homeClass = screenClass
end

function app.goHome()
    if app.homeClass then
        app.setScreen(app.homeClass:new())
    end
end

function app.hasUnsavedChanges(actionType)
    if app.screen.unsaved then return true end
    
    if actionType == "quit" then
        for _, v in ipairs(app.screenStack) do
            if v.unsaved then return true end
        end
    end
    
    return false
end

function app.drawScreen()
    love.graphics.clear(app.screen.bgRed, app.screen.bgGreen, app.screen.bgBlue)
    love.graphics.origin()

    if app.screen then
        app.screen:draw()
    end

    love.graphics.present()
end

function app.runScreen()
    love.timer.step()

    repeat
        love.event.pump()
        for e,a,b,c,d in love.event.poll() do
            _handleEvent(e, a, b, c, d)
        end
        
        local dt = 0

        -- Get the time delta...
        if love.timer then
            love.timer.step()
            dt = love.timer.getDelta()
        end

        app.screen:update(dt)

        if app.screen.dirty and love.graphics then
            app.drawScreen()
        end

        app.screen.dirty = false
    until not app.screen.animating or app.newScreen ~= app.screen or not app.running
end

function app.loadResources()
    app.res =
    {
        uifont = love.graphics.newFont("res/Lato-Regular.ttf", 16),
        uifontSmall = love.graphics.newFont("res/Lato-Regular.ttf", 13),
        uifontSemilarge = love.graphics.newFont("res/Lato-Regular.ttf", 24),
        uifontLarge = love.graphics.newFont("res/Lato-Regular.ttf", 32),
        alphaGradient = love.graphics.newImage("res/alphagradient256.png"),
    }

    love.graphics.setDefaultFilter("linear", "nearest", 0)
end

function love.run()
    if love.window then love.window.setDisplaySleepEnabled(true) end

    if love.math then
        love.math.setRandomSeed(os.time())
        for _ = 1, 100 do
            love.math.random()
        end
    end

    if love.event then
        love.event.pump()
    end

    cabin.addObserver(_standardOut, "info")

    if love.load then love.load(arg) end

    if love.keyboard then love.keyboard.setKeyRepeat(true) end

    if love.timer then love.timer.step() end

    if love.graphics then
        love.graphics.present()
    end

    -- Main loop time.
    while app.running do
        if app.newScreen then
            app.screen = app.newScreen
            app.newScreen = nil
            app.runScreen()
        end

        -- Process events.
        if love.event then
            _handleEvent(love.event.wait())

            if app.screen then
                app.runScreen()
            end
        end
    end
end

return app
