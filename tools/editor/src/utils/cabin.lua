--[[
See LICENSE for important information on distribution.
--]]


--[[
Cabin - A set of small logging helpers.
--]]

local _verbosityTable = {
    ['debug'] = 0,
    ['info'] = 1,
    ['warning'] = 2,
    ['error'] = 3
}

local cabin = {}

local _observers = {}


--[[
PRIVATE FUNCTIONS
--]]

local function _broadcast(message, severity)
    local severityNum = _verbosityTable[severity]
    
    if type(message) ~= 'string' then
        error("Message is not a string.")
    elseif not severityNum then
        error("Unknown severity level \"" .. tostring(severity) .. "\"")
    end
    
    for func, verbosity in pairs(_observers) do
        if verbosity <= severityNum then
            func(message, severity, severityNum)
        end
    end
end

--[[
PUBLIC STATICS
--]]

function cabin.addObserver(callbackFunc, verbosityLevel)
    if type(callbackFunc) ~= 'function' then
        error("Callback is not a function.")
    elseif not _verbosityTable[verbosityLevel] then
        error("Unknown verbosity level \"" .. tostring(verbosityLevel) .. "\"")
    end
    
    _observers[callbackFunc] = _verbosityTable[verbosityLevel]
end

function cabin.debug(message)
    _broadcast(message, 'debug')
end

function cabin.info(message)
    _broadcast(message, 'info')
end

function cabin.warning(message)
    _broadcast(message, 'warning')
end

function cabin.error(message)
    _broadcast(message, 'error')
end

return cabin
