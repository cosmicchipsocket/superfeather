
local bit = require "bit"

local binutil = {}

function binutil.readWord(dataString, pos)
    local binValue = bit.tobit(dataString:byte(pos+1))
    
    binValue = bit.lshift(binValue, 8)
    binValue = bit.bor(binValue, dataString:byte(pos))
    
    return binValue
end

function binutil.writeWord(binValue)
    local lowByte = bit.band(binValue, 255)
    local highByte = bit.rshift(binValue, 8)
    local dataString = string.char(lowByte, highByte)
    
    return dataString
end

return binutil
