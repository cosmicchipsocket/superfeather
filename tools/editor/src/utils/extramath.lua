
local extramath = {}

function extramath.clampValue(minVal, maxVal, value)
    return math.min(maxVal, math.max(minVal, value))
end

function extramath.round(value)
    return math.floor(value + 0.5)
end

return extramath
