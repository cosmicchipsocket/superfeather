--[[
See LICENSE for important information on distribution.
--]]


--[[
Test - functionality relating to code testing
--]]

local test = {}

function test.unusedArgs() end

function test.implementMe() error("Unimplemented function", 2) end

return test
