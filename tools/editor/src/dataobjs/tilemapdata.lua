local bit = require "bit"

local cabin = require "src.utils.cabin"
local binutil = require "src.utils.binutil"

local DataObject = require "src.dataobjs.dataobj"

local TilemapData = DataObject:makeClass
{
    
}

function TilemapData.wordToTile(tileWord)
    local newTile = {}
    
    newTile.index = bit.band(tileWord, 1023)
    newTile.hflip = (bit.band(tileWord, 16384) > 0)
    newTile.vflip = (bit.band(tileWord, 32768) > 0)
    newTile.priority = (bit.band(tileWord, 8192) > 0)
    newTile.palette = bit.rshift(tileWord, 10)
    newTile.palette = bit.band(newTile.palette, 7)
    
    return newTile
end

function TilemapData.tileToWord(tile)
    local tileWord = math.floor(tile.index % 1024)
    
    tileWord = bit.bor(tileWord, tile.palette * 1024)
    tileWord = tile.priority and tileWord + 8192 or tileWord
    tileWord = tile.hflip and tileWord + 16384 or tileWord
    tileWord = tile.vflip and tileWord + 32768 or tileWord
    
    return tileWord
end

function TilemapData:loadData(fileData)
    self.tiles = {}
    
    for i = 1, fileData:len() - 1, 2 do
        local tileWord = binutil.readWord(fileData, i)
        
        local newTile = TilemapData.wordToTile(tileWord)
        
        table.insert(self.tiles, newTile)
    end
    
    cabin.info("Tilemap has " .. #self.tiles .. " tiles")
end

function TilemapData:saveData()
    local fileData = ""
    
    for _, v in ipairs(self.tiles) do
        local tileWord = TilemapData.tileToWord(v)
        
        fileData = fileData .. binutil.writeWord(tileWord)
    end
    
    return fileData
end

return TilemapData
