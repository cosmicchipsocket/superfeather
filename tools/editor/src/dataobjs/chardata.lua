local bit = require "bit"

local cabin = require "src.utils.cabin"

local DataObject = require "src.dataobjs.dataobj"

local CharData = DataObject:makeClass
{
    
}

function CharData:loadData(fileData)
    if self.filepath:find(".pic2", -5) then
        self.bitDepth = 2
    else
        self.bitDepth = 4
    end
    
    self.tiles = {}
    
    local tileSize = 8 * self.bitDepth
    local numTiles = math.floor(fileData:len() / tileSize)
    local dataPos = 0
    
    for i = 1, numTiles do
        local tile = {}
        
        -- Initialize
        for x = 0, 63 do
            tile[x] = 0
        end
        
        local planeBit = 0
        
        -- Sequential planes
        for _ = 1, self.bitDepth / 2 do
            for y = 0, 7 do
                -- Interleaved planes
                for k = 0, 1 do
                    dataPos = dataPos + 1
                    
                    -- Row
                    local bitRow = bit.tobit(fileData:byte(dataPos))
                    
                    for x = 0, 7 do
                        if bit.band(bitRow, 2^(7-x)) ~= 0 then
                            local tilePos = x + 8*y
                            tile[tilePos] = tile[tilePos] + (2^(planeBit+k))
                        end
                    end
                end
            end
            
            planeBit = planeBit + 2
        end
        
        self.tiles[i] = tile
    end
    
    cabin.info("Char data read as " .. self.bitDepth .. " bitplanes, " .. numTiles .. " tiles.")
end

function CharData:generateTexture(paletteData, columnCount, paletteOffset)
    if not self.bitDepth then return end
    
    local imageData, transparentTiles = self:generateImageData(paletteData, columnCount, paletteOffset)
    
    return love.graphics.newImage(imageData), transparentTiles
end

function CharData:generateImageData(paletteData, columnCount, paletteOffset)
    paletteOffset = paletteOffset or 0
    local neededColors = paletteOffset + 2^self.bitDepth
    
    if not paletteData or not paletteData.colors then
        error("No palette data sent.", 1)
    elseif #paletteData.colors < neededColors then
        error("Palette does not have enough colors. Expected at least " .. neededColors .. ", got "
              .. #paletteData.colors, 1)
    end
    
    local textureWidth = columnCount * 8
    local textureHeight = math.ceil(#self.tiles / columnCount) * 8
    
    local imageData = love.image.newImageData(textureWidth, textureHeight)
    local transparentTiles = {}
    
    for i, tile in ipairs(self.tiles) do
        local originX = ((i - 1) % columnCount) * 8
        local originY = (math.floor((i - 1) / columnCount)) * 8
        local transparent = true
        
        for y = 0, 7 do
            for x = 0, 7 do
                local colorIndex = tile[x + 8*y]
                
                if colorIndex ~= 0 then
                    transparent = false
                    local color = paletteData.colors[colorIndex + paletteOffset + 1]
                    imageData:setPixel(originX + x, originY + y,
                                       color.red / 31 * 255, color.green / 31 * 255, color.blue / 31 * 255, 255)
                end
            end
        end
        
        if transparent then
            transparentTiles[i - 1] = true
        end
    end
    
    return imageData, transparentTiles
end

return CharData
