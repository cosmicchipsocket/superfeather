
.p816   ; 65816 processor
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/audio.inc"

.import InitializeSNES, GameMain, SpcDriver
.import sceneInitFunc, sceneThinkFunc, screenBrightness, BeginSprites, UpdateObjects, FinalizeSprites

.export Reset

.segment "CODE0"

_DefaultSceneInit:
    rts

_DefaultSceneThink:
    php
    
    sep #$20
    lda #%00001111
    sta screenBrightness
    rep #$30
    
    jsr BeginSprites
    jsr UpdateObjects
    jsr FinalizeSprites
    
    plp
    rts

Reset:
    jml FastStart
FastStart:
    
    ; From InitSNES, original by Neviksti
    
    sei             ; Disable interrupts
    clc             ; Switch to native mode
    xce

    rep #$38        ; mem/A = 16 bit, X/Y = 16 bit
                    ; Decimal mode off

    ldx #$1FFF      ; Setup the stack
    txs             ; Transfer index X to stack pointer register

    ; Do the rest of the initialization in a routine
    jsl InitializeSNES
    
    ; End InitSNES

    sep #$20
    
    lda #$80
    pha
    plb
    
    ; Enable FastROM
    lda #$01
    sta $420d
    ; Set interrupts
    ;     n yx   a
    lda #%10000001
    sta $4200
    
    Macro_SpcUpload SpcDriver, #$200, #$200
    Macro_SpcCommand #SPC_CMD_INITIALIZE, #$0000
    Macro_SpcCommand #SPC_CMD_STEREO, #$0001
    
    rep #$30
    
    lda #.loword(_DefaultSceneInit)
    sta sceneInitFunc
    lda #.loword(_DefaultSceneThink)
    sta sceneThinkFunc
    
    jml GameMain
