Superfeather SNES Game Engine
====

Make fast SNES games in assembly. Performant. Flexible. Convenient.

Note that this git repo does not contain any tool binaries. Those are shipped with releases.

# Current status #

Development of Superfeather is currently on hold while I focus my efforts on the
more general-purpose Keeshond Engine.

This isn't the end of Superfeather. I hope to return to this project at some
point and rework some of its aspects, most notably the editor.

## License ##

Superfeather uses the zlib License. See LICENSE for more details.
